import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by jonak on 12/16/2015.
 */
public class Main {

    public static void main(String[] args) {

        int top = 10000;
        int iterDenom = 10000;

        if(args.length == 2) {
            top = Integer.parseInt(args[0]);
            iterDenom = Integer.parseInt(args[1]);
        }
        assert(top % iterDenom == 0);

        long startTime = System.nanoTime();

        long[] valuStorer = new long[top / iterDenom];

        for(int i = 0; i < top; i += iterDenom) {
            valuStorer[i / iterDenom] = timedLoop(i);

            System.out.println((i / iterDenom) + 1 + "/" + (top / iterDenom));
        }

        toCSV(valuStorer, iterDenom, top, System.nanoTime() - startTime);


    }

    static void toCSV(long[] times, int denom, int top, long totalTime)
    {
        try {
            FileWriter fw = new FileWriter(decideName("val-norm", "csv"));

            fw.append("Iterations,Time to completion (ns),,Total Time (ns)" + "\n");
            fw.append(",,," + totalTime + "\n");
            fw.append(",,,," + "\n");

            for(int i = 0; i < top / denom; i++) {
                fw.append(i * denom + "," + times[i] + "\n");
            }

            fw.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    static String decideName(String nameBase, String ext)
    {
        return decideName(nameBase, ext, 0);
    }

    static String decideName(String nameBase, String ext, int iter)
    {
        ext = ext.replaceAll("\\.", "");

        String filename = iter == 0 ? nameBase + "." + ext : nameBase + iter + "." + ext;

        File f = new File(filename);

        if(f.exists() || f.isDirectory()) {
            return decideName(nameBase, ext, iter + 1);
        } else if(iter == 0) {
            return nameBase + "." + ext;
        } else {
            return nameBase + iter + "." + ext;
        }
    }

    static long timedLoop(int iters) {
        long start = System.nanoTime();

        for (int i = 0; i < iters; i++) {

            isPrime(i);

        }

        return System.nanoTime() - start;

    }

    static boolean isPrime(int intToCheck)
    {
        if (intToCheck <= 1) return false;

        if (intToCheck % 2 == 0) return false;

        for(int i = 3; i < intToCheck; i += 2)
        {
            if (intToCheck % i == 0) return false;
        }

        return true;
    }

}
