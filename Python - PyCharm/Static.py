import os
import time

import sys


def is_prime(num):
    if num <= 1:
        return False
    if num % 2 == 0:
        return False
    i = 3
    while i < num:
        if num % i == 0:
            return False
        i += 1
    return True


def timed_loop(iters, n):
    start = time.time()

    for x in range(0, iters):
        is_prime(n)
    return time.time() - start


def output_csv(times, denom, top, total_time, base_name, ext, iter):
    if iter == 0:
        filename = base_name + "." + ext
    else:
        filename = base_name + str(iter) + "." + ext

    if os.path.isfile(filename):
        output_csv(times, denom, top, total_time, base_name, ext, iter + 1)
        return

    to_write = "Iterations,Time to completion (s),,Total Time (s)" + "\n"
    to_write += ",,," + str(total_time) + "\n"
    to_write += ",,,," + "\n"

    for i in range(0, int(top / denom)):
        to_write += str(i * denom) + "," + str(times[i]) + "\n"

    f = open(filename, "w")

    f.write(to_write)
    f.close()


def start():
    top = 10000000
    denom = 10000

    if len(sys.argv) == 3:
        top = int(sys.argv[1])
        denom = int(sys.argv[2])

    assert top % denom == 0

    start_time = time.time()

    stored_values = [0] * int(top / denom)

    i = 0

    while i < top:
        stored_values[int(i / denom)] = timed_loop(i, 1001)

        print(str((i / denom) + 1) + "/" + str(top / denom))

        i += denom

    output_csv(stored_values, denom, top, time.time() - start_time, "val-static", "csv", 0)

start()

