#include <iostream>
#include <fstream>
#include <assert.h>
#include <sstream>
#include <chrono>
#include <stdlib.h>

using namespace std;

bool isValidFile(string filename) {
    ifstream fileToCheck(filename);
    return fileToCheck.good();
}

void toCSV(int currentFileIterator, long times[], int denom, int top, long totalTime) {

    stringstream filename;
    if(currentFileIterator == 0) {
        filename << "val-norm.csv";
    } else {
        filename << "val-norm" << currentFileIterator << ".csv";
    }
    if(isValidFile(filename.str())) {
        toCSV(currentFileIterator + 1, times, denom, top, totalTime);
        return;
    }

    ofstream fw;

    fw.open(filename.str());

    stringstream ss;

    ss << "Iterations,Time to completion (ns),,Total Time (ns)" << endl;
    ss << ",,," << totalTime << endl;
    ss << ",,,," << endl;

    for (int i = 0; i < top / denom; i++) {
        int iter = i * denom;
        ss << iter << "," << times[i] << endl;
    }

    string finalString = ss.str();

    fw << finalString;

    fw.close();
}


bool isPrime(int intToCheck) {
    if (intToCheck <= 1) return false;

    if (intToCheck % 2 == 0) return false;

    for (int i = 3; i < intToCheck; i += 2) {
        if (intToCheck % i == 0) return false;
    }

    return true;
}

long timedLoop(int iters) {
    auto start = chrono::high_resolution_clock::now();

    for (int i = 0; i < iters; i++) {

        isPrime(i);

    }

    auto end = chrono::high_resolution_clock::now();

    long finalTime = chrono::duration_cast<chrono::nanoseconds>(end - start).count();

    return finalTime;

}

int main(int argc, char *argv[]) {

    int top = 1000000;
    int iterDenom = 10000;

    if (argc > 2) {
        top = atoi(argv[1]);
        iterDenom = atoi(argv[2]);
    }

    auto startTime = chrono::high_resolution_clock::now();

    assert(top % iterDenom == 0);

    long valuStorer[top / iterDenom];

    for (int i = 0; i < top; i += iterDenom) {
        valuStorer[i / iterDenom] = timedLoop(i);

        stringstream ss;

        ss << (i / iterDenom) + 1 << "/" << (top / iterDenom);

        cout << ss.str() << endl;
    }

    auto endTime = chrono::high_resolution_clock::now();

    long finalTime = chrono::duration_cast<chrono::nanoseconds>(endTime - startTime).count();

    toCSV(0, valuStorer, iterDenom, top, finalTime);

    return 0;
}